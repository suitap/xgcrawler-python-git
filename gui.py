#!/usr/bin/env python
# coding: utf-8

import Tkinter
import ttk
import threading
import sys
import logging

from crawler import XGCrawler
from _resources import DefaultEventListener, get_seqtype
from config import Config

__author__ = "suitap"
__version__ = "0.1.0"


class XGCrawlerFrame(ttk.Frame):
    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)
        self.master.title("XGCrawler" + " ver." + __version__)
        self.master.iconbitmap("resources/icon.ico")
        self.master.resizable(width=False, height=False)
        if sys.platform == "win32":
            self.pack(fill=Tkinter.X, expand=True, padx=5, pady=5)
        else:
            self.pack(fill=Tkinter.X, expand=True)

        self.GateFrame = ttk.LabelFrame(self, padding=5,
                                        text=u"eAMUSEMENT ログイン情報")
        self.NoteFrame = ttk.LabelFrame(self, padding=5,
                                        text=u"SkillSimulator ログイン情報")
        self.PartFrame = ttk.LabelFrame(self, padding=5,
                                        text=u"更新対象機種")
        self.SourceFrame = ttk.LabelFrame(self, padding=5,
                                          text=u"データ取得元")
        self.GateFrame.grid(row=0, column=0, padx=5, ipadx=2,
                            sticky=Tkinter.NW)
        self.NoteFrame.grid(row=1, column=0, padx=5, ipadx=2,
                            sticky=Tkinter.NW)
        self.PartFrame.grid(row=0, column=1, padx=5, ipadx=2,
                            sticky=Tkinter.NW)
        self.SourceFrame.grid(row=1, column=1, padx=5, ipadx=2,
                              sticky=Tkinter.NW)

        self.GateUsernameValue = Tkinter.StringVar()
        self.GatePasswordValue = Tkinter.StringVar()
        self.NoteUsernameValue = Tkinter.StringVar()
        self.NotePasswordValue = Tkinter.StringVar()
        self.GateUsernameLabel = ttk.Label(self.GateFrame, text="Username: ")
        self.GatePasswordLabel = ttk.Label(self.GateFrame, text="Password: ")
        self.GateUsernameText = ttk.Entry(self.GateFrame, cursor="xterm",
                                          textvariable=self.GateUsernameValue)
        self.GatePasswordText = ttk.Entry(self.GateFrame, show="*",
                                          cursor="xterm",
                                          textvariable=self.GatePasswordValue)
        self.NoteUsernameLabel = ttk.Label(self.NoteFrame, text="Username: ")
        self.NotePasswordLabel = ttk.Label(self.NoteFrame, text="Password: ")
        self.NoteUsernameText = ttk.Entry(self.NoteFrame, cursor="xterm",
                                          textvariable=self.NoteUsernameValue)
        self.NotePasswordText = ttk.Entry(self.NoteFrame, show="*",
                                          cursor="xterm",
                                          textvariable=self.NotePasswordValue)
        self.PartGuitarValue = Tkinter.BooleanVar()
        self.PartDrumValue = Tkinter.BooleanVar()
        self.PartGuitar = ttk.Checkbutton(self.PartFrame, text=u"GuitarFreaks",
                                          variable=self.PartGuitarValue)
        self.PartDrum = ttk.Checkbutton(self.PartFrame, text=u"DrumMania",
                                        variable=self.PartDrumValue)
        self.SourceValue = Tkinter.StringVar()
        self.SourceTargeted = ttk.Radiobutton(self.SourceFrame,
                                              text=u"スキル対象曲", value="Y",
                                              variable=self.SourceValue)
        self.SourceAll = ttk.Radiobutton(self.SourceFrame, text=u"全曲",
                                         value="N", variable=self.SourceValue)

        self.GateUsernameLabel.grid(row=0, column=0, sticky=Tkinter.E, pady=2)
        self.GatePasswordLabel.grid(row=1, column=0, sticky=Tkinter.E, pady=2)
        self.GateUsernameText .grid(row=0, column=1, sticky=Tkinter.W, pady=2)
        self.GatePasswordText .grid(row=1, column=1, sticky=Tkinter.W, pady=2)
        self.NoteUsernameLabel.grid(row=0, column=0, sticky=Tkinter.E, pady=2)
        self.NotePasswordLabel.grid(row=1, column=0, sticky=Tkinter.E, pady=2)
        self.NoteUsernameText .grid(row=0, column=1, sticky=Tkinter.W, pady=2)
        self.NotePasswordText .grid(row=1, column=1, sticky=Tkinter.W, pady=2)
        self.PartGuitar       .grid(row=0, column=0, sticky=Tkinter.W, pady=2)
        self.PartDrum         .grid(row=0, column=1, sticky=Tkinter.W, pady=2)
        self.SourceTargeted   .grid(row=0, column=0, sticky=Tkinter.W, pady=2)
        self.SourceAll        .grid(row=0, column=1, sticky=Tkinter.W, pady=2)

        self.SubmitButton = ttk.Button(self, text=u"開始", command=self.crawl)
        self.SubmitButton.grid(row=2, column=1, padx=5, pady=5, ipadx=20,
                               sticky=Tkinter.SE)

        self.StatusString = Tkinter.StringVar(self)
        self.StatusLabel = ttk.Label(self, textvariable=self.StatusString,
                                     width=60)
        self.StatusLabel.grid(row=3, column=0, columnspan=2, sticky=Tkinter.W)

        self.GateUsernameText.focus_set()

        self.config = Config("config.xml")
        self.config.load()
        self.load_from_config()

    def load_from_config(self):
        self.GateUsernameValue.set(self.config["EAGateUsername"])
        self.GatePasswordValue.set(self.config["EAGatePassword"])
        self.NoteUsernameValue.set(self.config["SkillNoteUsername"])
        self.NotePasswordValue.set(self.config["SkillNotePassword"])
        self.PartGuitarValue.set(self.config["UpdateGuitarSkill"] == "Y")
        self.PartDrumValue.set(self.config["UpdateDrumSkill"] == "Y")
        self.SourceValue.set("Y" if self.config["UpdateOnlySkillTarget"] == "Y" else "N")

    def save_to_config(self):
        self.config["EAGateUsername"] = self.GateUsernameValue.get()
        self.config["EAGatePassword"] = self.GatePasswordValue.get()
        self.config["SkillNoteUsername"] = self.NoteUsernameValue.get()
        self.config["SkillNotePassword"] = self.NotePasswordValue.get()
        if self.PartGuitarValue.get():
            self.config["UpdateGuitarSkill"] = "Y"
        else:
            self.config["UpdateGuitarSkill"] = "N"
        if self.PartDrumValue.get():
            self.config["UpdateDrumSkill"] = "Y"
        else:
            self.config["UpdateDrumSkill"] = "N"
        self.config["UpdateOnlySkillTarget"] = self.SourceValue.get()

    def crawl(self):
        self.save_to_config()
        listener = EventListener(self.StatusString, self.config)
        t = CrawlerThread(self, self.config, listener)
        t.setDaemon(True)
        t.start()

    def destroy(self):
        self.save_to_config()
        self.config.save()
        ttk.Frame.destroy(self)


class CrawlerThread(threading.Thread):
    def __init__(self, frame, config, listener):
        threading.Thread.__init__(self)
        self.frame = frame
        self.config = config
        self.listener = listener

    def run(self):
        self.frame.SubmitButton.configure(state=Tkinter.DISABLED)
        c = XGCrawler(self.config, self.listener)
        c.crawl()
        self.frame.SubmitButton.configure(state=Tkinter.NORMAL)


class EventListener(DefaultEventListener):
    def __init__(self, label, config):
        self.label = label
        self.config = config
        logging.getLogger().setLevel(logging.DEBUG)

        formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s",
                              "%Y-%m-%d %H:%M:%S")
        errorhandler = logging.FileHandler(filename="error.log",
                                           mode="w")
        errorhandler.setFormatter(formatter)
        logging.getLogger("xgcrawler.error").addHandler(errorhandler)

        if self.config["LoggingMusicTitle"] == "Y":
            titlehandler = logging.FileHandler(filename="musictitle.log",
                                               mode="w")
            formatter = logging.Formatter("%(message)s")
            titlehandler.setFormatter(formatter)
        else:
            titlehandler = logging.NullHandler()
        logging.getLogger("xgcrawler.musictitle").addHandler(titlehandler)

    def start(self):
        self.label.set(u"開始します")

    def end(self):
        self.label.set(u"終了しました")

    def login_gate(self):
        self.label.set(u"eAMUSEMENT にログインしています")

    def failed_login_gate(self):
        self.label.set(u"eAMUSEMENT へのログインに失敗しました")

    def login_note(self):
        self.label.set(u"Skill Simulator にログインしています")

    def failed_login_note(self):
        self.label.set(u"Skill Simulator へのログインに失敗しました")

    def start_collect(self):
        self.label.set(u"eAMUSEMENT からデータを取得しています")

    def collecting_music(self, gtype, name):
        logging.getLogger("xgcrawler.musictitle").info(
            u"[" + gtype + u"] " + name)

    def start_update(self):
        self.label.set(u"Skill Simulator の更新を開始します")

    def end_update(self):
        self.label.set(u"Skill Simulator の更新を終了します")

    def update_result(self, result):
        kwarg = {
            "music": result.gatetitle,
            "seqtype": get_seqtype(result.part, result.level)
        }
        self.label.set(u"{music}({seqtype})を更新しました".format(**kwarg))

    def collect_result(self, result):
        kwarg = {
            "music": result.gatetitle,
            "seqtype": get_seqtype(result.part, result.level)
        }
        self.label.set(u"{music}({seqtype})のデータを取得しています".format(**kwarg))

    def error(self, e):
        logging.getLogger("xgcrawler.error").exception(e.message)
        self.label.set(u"エラーが発生しました: {0}".format(e.message))

if __name__ == "__main__":
    f = XGCrawlerFrame()
    f.mainloop()
