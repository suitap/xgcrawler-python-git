import re
from collections import OrderedDict
from xml import dom
from xml.dom import minidom


class Config(object):

    def __init__(self, filename):
        self.filename = filename
        self.doc = None
        self.doc_anchor = OrderedDict()
        self.config = OrderedDict()
        self.config["EAGateUsername"] = ""
        self.config["EAGatePassword"] = ""
        self.config["SkillNoteUsername"] = ""
        self.config["SkillNotePassword"] = ""
        self.config["UpdateGuitarSkill"] = ""
        self.config["UpdateDrumSkill"] = ""
        self.config["UpdateOnlySkillTarget"] = ""
        # self.config["SynchronizeCompletely"] = ""
        # self.config["UpdateFullcombo"] = ""
        self.config["ProxyHost"] = ""
        self.config["ProxyPort"] = ""
        self.config["LoggingMusicTitle"] = ""

    def __getitem__(self, key):
        return remove_blank(self.config.__getitem__(key))

    def get(self, key, default):
        return remove_blank(self.config.get(key, default))

    def __setitem__(self, key, value):
        return self.config.__setitem__(key, remove_blank(value))

    def load(self):
        try:
            self.doc = minidom.parse(self.filename)
        except IOError:
            self.doc = minidom.parseString(template_xml)

        for elem in self.doc.getElementsByTagName("entry"):
            self.config[elem.getAttribute("key")] = get_text(elem)
            self.doc_anchor[elem.getAttribute("key")] = elem

    def save(self):
        existing_keys = self.doc_anchor.keys()
        root_node = self.doc.getElementsByTagName("properties")[0]
        for key in self.config.keys():
            if key in existing_keys:
                continue
            elem = self.doc.createElement("entry")
            elem.setAttribute("key", key)
            elem.appendChild(self.doc.createTextNode(self.config[key]))
            root_node.appendChild(self.doc.createTextNode("\t"))
            root_node.appendChild(elem)

        for elem in self.doc.getElementsByTagName("entry"):
            if elem.firstChild:
                elem.firstChild.replaceWholeText(
                    self.config.get(elem.getAttribute("key"), ""))
            else:
                elem.appendChild(self.doc.createTextNode(
                    self.config.get(elem.getAttribute("key"), "")))

        remove_whilespace_nodes(root_node)

        xml = re.sub(r"<entry(.+?)/>",
                     lambda (m): "<entry" + m.group(1) + "></entry>",
                     self.doc.toprettyxml(encoding="UTF-8"))

        f = open(self.filename, "w")
        f.write(xml)
        f.close()


def get_text(nodelist):
    rc = ""
    for node in nodelist.childNodes:
        if node.nodeType == node.TEXT_NODE:
            rc = rc + node.data
    return rc


def remove_whilespace_nodes(node):
    remove_list = []
    for child in node.childNodes:
        if child.nodeType == dom.Node.TEXT_NODE and not child.data.strip():
            remove_list.append(child)
        elif child.hasChildNodes():
            remove_whilespace_nodes(child)
    for node in remove_list:
        node.parentNode.removeChild(node)


def remove_blank(text):
    return blank_matcher.sub("", text)

blank_matcher = re.compile("\s")

template_xml = """<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
</properties>"""
