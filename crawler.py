#!/usr/bin/env python
# coding: utf-8

from decimal import Decimal

from asq.initiators import query
from asq.record import new
from asq.selectors import a_

import repository
from _resources import DefaultEventListener, CrawlerException


__author__ = "suitap"
__version__ = "0.1.0"


class XGCrawler(object):
    def __init__(self, config, listener=DefaultEventListener()):
        self.config = config
        self.browser = repository.Browser(config)
        self.repo = repository.Repository(self.browser, config, listener)
        self.listener = listener

    def setup_musics(self):
        self.musics = self.repo.setup_musics()

    def setup_difficulties(self):
        self.difficulties = self.repo.setup_difficulties()

    def collect_noteresults(self):
        return self.repo.collect_noteresults()

    def collect_targeted(self):
        results = self.repo.collect_targeted()
        return self.__build_query(results).to_list()

    def collect_all(self):
        results = self.repo.collect_all()
        return self.__build_query(results).order_by_descending(
            a_("gtype")
        ).then_by(
            a_("stype")
        ).then_by_descending(
            a_("skillpoint")
        ).distinct(
            lambda result: (result.title, result.gtype, result.stype)
        ).to_list()

    def crawl(self):
        try:
            self.listener.start()
            self.setup_musics()
            self.setup_difficulties()
            noteresults = self.collect_noteresults()
            results = list()
            if self.config["UpdateOnlySkillTarget"] == "Y":
                results += self.collect_targeted()
            else:
                results += self.collect_all()
            self.repo.update(results, noteresults)
            self.listener.end()
        except CrawlerException as e:
            pass
        except Exception as e:
            self.listener.error(e)

    def __build_query(self, list):
        return query(list).join(
            self.musics,
            lambda result: result.title,
            lambda music: music.title,
            lambda result, music: new(
                noteid=music.noteid,
                **result.__dict__
            )
        ).join(
            self.difficulties,
            lambda result: (result.title, result.level, result.part),
            lambda diffic: (diffic.title, diffic.level, diffic.part),
            lambda result, difficulty: new(
                difficulty=difficulty.value,
                stype=difficulty.stype,
                skillpoint=(
                    result.achievement *
                    difficulty.value *
                    Decimal("0.2")
                ).quantize(Decimal('.01'), rounding="ROUND_DOWN"),
                **result.__dict__
            )
        )


if __name__ == '__main__':
    pass
