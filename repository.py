#!/usr/bin/env python
# coding: utf-8

import cookielib
import decimal
import itertools
import urllib
import urllib2
import StringIO

import mechanize
from asq.initiators import query
from asq.record import new
from scrapemark import scrape

from _resources import (patterns as P, urls as U,
                        normalize, get_achievement, get_note_seqtype,
                        get_level, get_part, generate_seqtype,
                        difficulty_matcher, note_seqtype_matcher,
                        CrawlerException)


class Browser(object):
    def __init__(self, config):
        self.cookiejar = cookielib.CookieJar()
        handlers = []
        handlers.append(ValidHTMLHandler())
        handlers.append(urllib2.HTTPCookieProcessor(self.cookiejar))
        self.handlers = handlers
        self.browser = mechanize.Browser()
        self.browser.set_cookiejar(self.cookiejar)
        self.config = config
        self.setup_proxy()

    def setup_proxy(self):
        if (self.config.get("ProxyHost", "") and
                self.config.get("ProxyPort", "")):
            proxy_url = self.config["ProxyHost"] + ":"
            proxy_url += self.config["ProxyPort"]
            self.handlers.append(urllib2.ProxyHandler({
                'http': proxy_url, 'https': proxy_url,
            }))

    def scrape(self, pattern, url, **kwarg):
        return scrape(pattern, url=url, handlers=self.handlers, **kwarg)


class Gate(object):
    def __init__(self, browser, config, listener):
        self.browser = browser
        self.config = config
        self.listener = listener

    def __enter__(self):
        self.login()

    def __exit__(self, exc_type, exc_value, traceback):
        self.logout()
        return False

    def login(self):
        self.listener.login_gate()
        login_username = self.browser.scrape(
            """<p class="id_text">{{  }}</p>""",
            url=U["gate_login"],
            post={"KID": self.config["EAGateUsername"],
                  "pass": self.config["EAGatePassword"],
                  "OTP": ""})
        if not login_username:
            self.listener.failed_login_gate()
            raise CrawlerException
        if self.config["EAGateUsername"] != login_username:
            self.listener.failed_login_gate()
            raise CrawlerException

    def logout(self):
        self.browser.scrape(
            """<dummy>{{}}</dummy>""",
            url=U["gate_logout"])


class Note(object):
    def __init__(self, browser, config, listener):
        self.browser = browser
        self.config = config
        self.listener = listener

    def __enter__(self):
        self.login()

    def __exit__(self, exc_type, exc_value, traceback):
        self.logout()
        return False

    def login(self):
        self.listener.login_note()
        login_username = self.browser.scrape(
            """<div>{{  }}<br>""",
            url=U["note_login"],
            post={"id": self.config["SkillNoteUsername"],
                  "passwd": self.config["SkillNotePassword"]})
        if not login_username:
            self.listener.failed_login_note()
            raise CrawlerException
        if self.config["SkillNoteUsername"] not in login_username:
            self.listener.failed_login_note()
            raise CrawlerException

    def logout(self):
        self.browser.scrape(
            """<dummy>{{}}</dummy>""",
            url=U["note_logout"])


class Repository(object):

    def __init__(self, browser, config, listener):
        self.browser = browser
        self.config = config
        self.listener = listener
        self.gate = Gate(browser, config, listener)
        self.note = Note(browser, config, listener)

    def setup_musics(self):
        target = list()
        with self.note:
            self.browser.scrape(
                """<dummy>{{}}</dummy>""",
                url=U["note_gfmode"])
            r = self.browser.scrape(
                """{*<option value='{{ [id] }}'>{{ [music] }}</option>*}""",
                url=U["note_gfinfo"])
            for i in range(0, len(r["music"])):
                target.append(new(
                    title=normalize(r["music"][i]),
                    noteid=r["id"][i],
                ))
            target.append(new(
                title=u"dependonme",  # :-<
                noteid="219",
            ))
        return target

    def setup_difficulties(self):
        target = list()
        for stype in ["new", "old"]:
            r = self.browser.scrape(
                P["note_musiclist_pattern"],
                url=U["note_musiclist"].format(stype=stype))
            for i, (level, part, seqtype) in generate_seqtype(len(r["title"])):
                difficulty = r[seqtype][i]
                if difficulty_matcher.search(difficulty):
                    difficulty = decimal.Decimal(difficulty)
                    target.append(new(
                        title=normalize(r["title"][i]),
                        level=level,
                        part=part,
                        stype=stype,
                        value=difficulty,
                    ))
        return target

    def collect_noteresults(self):
        target = list()
        for gtype in ["gf", "dm"]:
            r = self.browser.scrape(
                P["note_detail_pattern"],
                url=U["note_" + gtype + "list"].format(
                    uid=self.config["SkillNoteUsername"]))
            for i in range(0, len(r["music"])):
                m = note_seqtype_matcher.search(r["diffseqtype"][i])
                level = m.group(1)
                part = m.group(2) if m.group(2) else "D"
                target.append(new(
                    title=normalize(r["music"][i]),
                    gtype=gtype,
                    level=level,
                    part=part,
                    achievement=decimal.Decimal(r["achievement"][i]),
                    skillpoint=decimal.Decimal(r["skillpoint"][i]),
                ))
        return target

    def collect_targeted(self):
        gtypes, target = list(), list()
        with self.gate:
            self.listener.start_collect()
            if self.config["UpdateGuitarSkill"] == "Y":
                gtypes.append("gf")
            if self.config["UpdateDrumSkill"] == "Y":
                gtypes.append("dm")
            for gtype, stype in itertools.product(gtypes, [1, 0]):
                url = U["gate_playdata"].format(gtype=gtype, stype=stype)
                results = self.browser.scrape(P["gate_targeted"], url=url)
                for i in range(0, len(results["music"])):
                    self.listener.collecting_music(
                        gtype, results["music"][i])
                    achievement = get_achievement(results["achievement"][i])
                    rs = new(
                        title=normalize(results["music"][i]),
                        gatetitle=results["music"][i],
                        gtype=gtype,
                        level=get_level(results["level"][i]),
                        part=get_part(results["part"][i]),
                        achievement=achievement,
                        highscore="",
                        maxcombo="",
                        rank=""
                    )
                    target.append(rs)
                    self.listener.collect_result(rs)
        self.listener.end_collect()
        return target

    def collect_all(self):
        gtypes, target = list(), list()
        with self.gate:
            self.listener.start_collect()
            if self.config["UpdateGuitarSkill"] == "Y":
                gtypes.append("gf")
            if self.config["UpdateDrumSkill"] == "Y":
                gtypes.append("dm")
            for gtype, category in itertools.product(gtypes, range(0, 14)):
                detail_urls = self.browser.scrape(
                    P["gate_detaillist"],
                    url=U["gate_playdata_detaillist"].format(
                        gtype=gtype,
                        category=category))
                for i in range(0, len(detail_urls["url"])):
                    self.listener.collecting_music(
                        gtype, detail_urls["title"][i])
                    detail_url = detail_urls["url"][i]
                    results = self.browser.scrape(
                        P["gate_detail" + gtype],
                        url=detail_url)
                    title = normalize(results["music"])
                    for i in range(0, len(results["level"])):
                        achievement = get_achievement(
                            results["achievement"][i])
                        if gtype == "gf":
                            part = get_part(results["part"][i])
                        else:
                            part = "D"
                        rs = new(
                            title=title,
                            gatetitle=results["music"],
                            gtype=gtype,
                            level=get_level(results["level"][i]),
                            part=part,
                            achievement=achievement,
                            highscore=results["highscore"][i],
                            maxcombo=results["maxcombo"][i],
                            rank=results["rank"][i],
                        )
                        target.append(rs)
                        self.listener.collect_result(rs)
        self.listener.end_collect()
        return target

    def update(self, results, noteresults):
        self.listener.start_update()
        with self.note:
            for (configkey, url, gtype) in [
                    ("UpdateGuitarSkill", U["note_gfmode"], "gf"),
                    ("UpdateDrumSkill", U["note_dmmode"], "dm")]:
                if self.config[configkey] != "Y":
                    continue
                self.browser.scrape(
                    """<dummy>{{}}</dummy>""",
                    url=url)
                for result in query(results).where(
                        lambda r: r.gtype == gtype):
                    if query(noteresults).any(
                            lambda n, result=result:
                            n.title == result.title and
                            n.gtype == result.gtype and
                            n.skillpoint == result.skillpoint):
                        continue
                    qs = "?music_id=" + result.noteid
                    b = self.browser.browser
                    b.open(U["note_skilledit_base"] + qs)
                    b.select_form(nr=0)
                    b["seq_type"] = (get_note_seqtype(
                        result.part, result.level),)
                    b["acv"] = str(result.achievement)
                    b.submit()
                    self.listener.update_result(result)
        self.listener.end_update()


class ValidHTMLHandler(urllib2.BaseHandler):
    handler_order = 600

    def http_response(self, req, res):
        if not (200 <= res.code < 300):
            return res  # pass through

        body = StringIO.StringIO(res.read())
        if "</html>" not in body.getvalue():
            raise urllib2.URLError("received data is invalid!")
        resp = urllib.addinfourl(body, res.info(), res.geturl())
        resp.code = res.code
        resp.msg = res.msg
        return resp

    https_response = http_response
