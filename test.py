#!/usr/bin/env python
# coding: utf-8

import unittest

import _resources
import config
import repository


class TestResources(unittest.TestCase):

    def test_normalize(self):
        test = lambda input, output: (
            self.assertEquals(_resources.normalize(input), output))
        test(u"CaptivAte～裁き～",
             u"captivate~裁き~")
        test(u"量子の海のリントヴルム（GITADOROCK Ver.）",
             u"量子の海のリントヴルム(gitadorockver.)")
        test(u"WILD CREATRURES",
             u"wildcreatrures")
        test(u"WILD CREATURES",
             u"wildcreatrures")
        test(u"†渚の小悪魔ラヴリィ～レイディオ†",
             u"†渚の小悪魔ラヴリィ~レイディオ†(gitadoraver.)")
        test(u"†渚の小悪魔ラヴリィ～レイディオ† (GITADORA ver.)",
             u"†渚の小悪魔ラヴリィ~レイディオ†(gitadoraver.)")
        test(u"ZЁNITH",
             u"zenith")
        test(u"RЁVOLUTIΦN",
             u"revolution")

    def test_get_note_seqtype(self):
        test = lambda part, level, seqtype: (
            self.assertEquals(_resources.get_note_seqtype(part, level), seqtype))
        test("GUITAR", "BASIC", "BSC(G)")
        test("BASS", "BASIC", "BSC(B)")
        test("DRUM", "BASIC", "BSC")
        test("GUITAR", "ADVANCE", "ADV(G)")
        test("BASS", "ADVANCE", "ADV(B)")
        test("DRUM", "ADVANCE", "ADV")
        test("GUITAR", "EXTREME", "EXT(G)")
        test("BASS", "EXTREME", "EXT(B)")
        test("DRUM", "EXTREME", "EXT")
        test("GUITAR", "MASTER", "MAS(G)")
        test("BASS", "MASTER", "MAS(B)")
        test("DRUM", "MASTER", "MAS")

    def test_get_seqtype(self):
        test = lambda part, level, seqtype: (
            self.assertEquals(_resources.get_seqtype(part, level), seqtype))
        test("GUITAR", "BASIC", "BSC-G")
        test("BASS", "BASIC", "BSC-B")
        test("DRUM", "BASIC", "BSC-D")
        test("GUITAR", "ADVANCE", "ADV-G")
        test("BASS", "ADVANCE", "ADV-B")
        test("DRUM", "ADVANCE", "ADV-D")
        test("GUITAR", "EXTREME", "EXT-G")
        test("BASS", "EXTREME", "EXT-B")
        test("DRUM", "EXTREME", "EXT-D")
        test("GUITAR", "MASTER", "MAS-G")
        test("BASS", "MASTER", "MAS-B")
        test("DRUM", "MASTER", "MAS-D")


class TestGate(unittest.TestCase):

    def setUp(self):
        self.config = config.Config("config.xml")
        self.config.load()
        self.browser = repository.Browser(self.config)
        self.gate = repository.Gate(self.browser, self.config)

    def test_login(self):
        self.gate.login()
        session_string = self.gate.browser.cookiejar._cookies["p.eagate.573.jp"]["/"]["M573SSID"].value
        self.assertTrue(session_string)

    def test_logout(self):
        self.gate.logout()


class TestNote(unittest.TestCase):

    def setUp(self):
        self.config = config.Config("config.xml")
        self.config.load()
        self.browser = repository.Browser(self.config)
        self.note = repository.Note(self.browser, self.config)

    def test_login(self):
        self.note.login()
        session_string = self.note.browser.cookiejar._cookies["xv-s.heteml.jp"]["/"]["PHPSESSID"].value
        self.assertTrue(session_string)

    def test_logout(self):
        self.note.logout()


if __name__ == '__main__':
    unittest.main()
