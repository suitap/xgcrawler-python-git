#!/usr/bin/env python
# coding: utf-8

import decimal
import itertools
import re
import unicodedata


patterns = {}

patterns["gate_targeted"] = """
    <li class="select_{{gf}}"><a>GuitarFreaks</a></li>
    <li class="select_{{dm}}"><a>DrumMania</a></li>
    <li class="select_{{new}}"><a>HOT</a></li>
    <li class="select_{{old}}"><a>EXISTING</a></li>
    <table class="skill_table">
    {*
    <a class="text_link">{{ [music] }}</a>
    <div class="seq_icon part_{{ [part] }}"></div>
    <div class="seq_icon diff_{{ [level] }}"></div>
    <td class="skill_cell">{{ [skillpoint] }}<span></span></td>
    <td class="achive_cell">{{ [achievement] }}</td>
    *}"""

patterns["gate_detaillist"] = """
    {*
    <div class="md_title_box"><a class="text_link" href="{{ [url]|abs }}">{{ [title] }}</a>
    *}"""

patterns["gate_detailgf"] = """
    <div class="live_title">{{ music }}</div>
    {*
    <div class="index_md gf"><font>{{ [part] }}</font>
     / <font>{{ [level] }}</font></div>
    <table><tbody>
    <tr><td></td><td></td></tr>
    <tr><td></td><td></td></tr>
    <tr><td></td><td>{{ [rank] }}</td></tr>
    <tr><td></td><td>{{ [achievement] }}</td></tr>
    <tr><td></td><td>{{ [highscore] }}</td></tr>
    <tr><td></td><td>{{ [maxcombo] }}</td></tr>
    </tbody></table>
    *}"""

patterns["gate_detaildm"] = """
    <div class="live_title">{{ music }}</div>
    {*
    <div class="index_md gf"><font></font>
    <font>{{ [level] }}</font></div>
    <table><tbody>
    <tr><td></td><td></td></tr>
    <tr><td></td><td></td></tr>
    <tr><td></td><td>{{ [rank] }}</td></tr>
    <tr><td></td><td>{{ [achievement] }}</td></tr>
    <tr><td></td><td>{{ [highscore] }}</td></tr>
    <tr><td></td><td>{{ [maxcombo] }}</td></tr>
    </tbody></table>
    *}"""

patterns["note_musiclist_pattern"] = """
    {*
    <tr>
    <td id=etc class=l>{{ [title] }}</td>
    <td> </td>
    <td id=nov>{{ [bscd] }}</td>
    <td id=reg>{{ [advd] }}</td>
    <td id=exp>{{ [extd] }}</td>
    <td id=mas>{{ [masd] }}</td>
    <td id=nov>{{ [bscg] }}</td>
    <td id=reg>{{ [advg] }}</td>
    <td id=exp>{{ [extg] }}</td>
    <td id=mas>{{ [masg] }}</td>
    <td id=nov>{{ [bscb] }}</td>
    <td id=reg>{{ [advb] }}</td>
    <td id=exp>{{ [extb] }}</td>
    <td id=mas>{{ [masb] }}</td>
    <td id=etc class=l></td>
    </tr>
    *}"""

patterns["note_detail_pattern"] = """
    {*
    <tr><td></td><td id=l>{{ [music] }}</td>
    <td id=l>{{ [diffseqtype] }}</td>
    <td>{{ [achievement] }}%</td>
    <td>{{ [skillpoint] }}</td>
    <td></td><td></td></tr>
    *}"""


urls = {}

urls["gate_login"] = "https://p.eagate.573.jp/gate/p/login.html"
urls["gate_playdata"] = "http://p.eagate.573.jp/game/gfdm/gitadora/p/cont/play_data/skill.html?gtype={gtype}&stype={stype}"  # NOQA
urls["gate_playdata_detaillist"] = "http://p.eagate.573.jp/game/gfdm/gitadora/p/cont/play_data/music.html?gtype={gtype}&cat={category}"  # NOQA
urls["gate_logout"] = "http://eagate.573.jp/gate/p/logout.html"

urls["note_login"] = "http://xv-s.heteml.jp/skill/logintop_gd.php"
urls["note_musiclist"] = "http://xv-s.heteml.jp/skill/music_gd.php?k={stype}"
urls["note_gfmode"] = "http://xv-s.heteml.jp/skill/skilledit_gd.php?gd=g"
urls["note_dmmode"] = "http://xv-s.heteml.jp/skill/skilledit_gd.php?gd=d"
urls["note_gfinfo"] = "http://xv-s.heteml.jp/skill/skilledit_info_gd.php?gd=g"
urls["note_skilledit_base"] = "http://xv-s.heteml.jp/skill/acvedit_gd.php"
urls["note_logout"] = "http://xv-s.heteml.jp/skill/logout_gd.php"
urls["note_gflist"] = "http://xv-s.heteml.jp/skill/gd.php?uid=g{uid}"
urls["note_dmlist"] = "http://xv-s.heteml.jp/skill/gd.php?uid=d{uid}"


difficulty_matcher = re.compile(r"^\d\.\d\d$")
note_seqtype_matcher = re.compile(r"\S+ (BSC|ADV|EXT|MAS)(?:\((G|B)\))?")


class DefaultEventListener(object):
    def start(self):
        pass

    def end(self):
        pass

    def login_gate(self):
        pass

    def failed_login_gate(self):
        pass

    def login_note(self):
        pass

    def failed_login_note(self):
        pass

    def start_collect(self):
        pass

    def end_collect(self):
        pass

    def collecting_music(self, gtype, name):
        pass

    def start_update(self):
        pass

    def end_update(self):
        pass

    def update_result(self, result):
        pass

    def collect_result(self, result):
        pass

    def error(self, e):
        pass


class CrawlerException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)


def normalize(text):
    target = unicode(re.sub(r"\\s", "", text))
    normalized = unicodedata.normalize("NFKC", target)
    normalized = normalized.replace(u"Ё", "E")
    normalized = normalized.replace(u"Φ", "O")
    normalized = normalized.replace(u"　", "")
    normalized = normalized.replace(u" ", "")
    normalized = normalized.replace(u"WILDCREATURES", u"WILDCREATRURES")
    normalized = re.sub(ur"レイディオ†$", u"レイディオ†(GITADORAver.)",
                        normalized)
    normalized = normalized.lower()
    return normalized


def get_achievement(text):
    achievement = text.replace("%", "")
    achievement = achievement.replace("NO", "0.00")
    achievement = achievement.replace("MAX", "100.00")
    return decimal.Decimal(achievement)


def get_note_seqtype(part, level):
    if get_part(part) == "D":
        return get_level(level)
    else:
        return get_level(level) + "(" + get_part(part) + ")"


def get_seqtype(part, level):
    return get_level(level) + "-" + get_part(part)


def get_level(level):
    if level == "BASIC":
        str = "BSC"
    else:
        str = level[0:3]
    return str


def get_part(part):
    return part[0:1]


def generate_seqtype(size):
    return itertools.product(
        range(0, size),
        map(
            (lambda x, y: (x, y, (x + y).lower())),
            ("BSC", "ADV", "EXT", "MAS",) * 3,
            ("D",) * 4 + ("G",) * 4 + ("B",) * 4,
        )
    )
