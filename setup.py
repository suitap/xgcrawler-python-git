from cx_Freeze import setup, Executable

from distutils.cmd import Command
import os
import zipfile
import urllib
import crawler


class PackageCommand(Command):
    description = 'create release package'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        self.base_filename = "%s-%s" % (self.distribution.metadata.name,
                                        self.distribution.metadata.version)
        self.target_dir = self.get_finalized_command('build').build_exe
        self.dest_dir = self.get_finalized_command('build').build_base

    def run(self):
        urllib.URLopener().retrieve(
            "http://suitap.iceextra.org/xgcrawler/temp/readme.txt",
            os.path.join(self.target_dir, "readme.txt"))
        zipfilepath = os.path.join(self.dest_dir, self.base_filename + ".zip")
        zip = zipfile.ZipFile(zipfilepath, 'w', zipfile.ZIP_DEFLATED)
        zip.comment = self.base_filename
        rootlen = len(self.target_dir) + 1
        for base, dirs, files in os.walk(self.target_dir):
            for file in files:
                if file == "config.xml":
                    continue
                fn = os.path.join(base, file)
                zip.write(fn, fn[rootlen:])


setup(
    name="XGCrawler",
    version=crawler.__version__,
    description="XGCrawler",
    author=crawler.__author__,
    url="https://bitbucket.org/suitap/xgcrawler-python",
    executables=[
        Executable(
            "gui.py",
            base="Win32GUI",
            icon="resources/icon.ico",
            targetName="XGCrawler.exe",
        )],
    options={
        "build_exe": {
            "excludes": ["sqlite3"],
            "include_files": ["resources/icon.ico",],
        },
    },
    cmdclass={
        "package": PackageCommand,
    },
)
